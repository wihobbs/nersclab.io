# NERSC Technical Documentation

!!! tip "NERSC welcomes your contributions"
	These pages are hosted from a
	[git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
	[contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/main/CONTRIBUTING.md)
	are welcome!

	[Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

## NERSC web pages

* [NERSC Home page](https://nersc.gov) - center news and information
* [MyNERSC](https://my.nersc.gov) - interactive content
* [Help Portal](https://help.nersc.gov) - open tickets, make requests
* [JupyterHub](https://jupyter.nersc.gov) - access NERSC with interactive notebooks and more

## Popular documentation pages

* [Job Queue Policy](jobs/policy.md) - charge factors, run limits, submit limits
* [Example Jobs](jobs/examples/index.md) - curated example job scripts
* [Jobs overview](jobs/index.md) - Slurm commands, job script basics, submitting, updating jobs
* [Jupyter](services/jupyter.md) - interactive jupyter notebooks at NERSC
* [Globus](services/globus.md) - high performance data transfers
* [File permissions](filesystems/unix-file-permissions.md) - Unix file permissions
* [Multi-Factor Authentication](connect/mfa.md)
