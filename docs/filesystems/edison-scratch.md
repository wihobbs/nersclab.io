# Edison SCRATCH

Edison was decommissioned on May 13, 2019. All data on Edison's
scratch file systems (/scratch1, /scratch2, and /scratch3) has been
destroyed.
