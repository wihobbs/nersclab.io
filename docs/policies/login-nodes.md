# NERSC Login Node Policy

## Usage

!!! warning "Appropriate Use"
    Do not run compute- or memory-intensive applications on login
    nodes. These nodes are a shared resource. NERSC may terminate
    processes which are having negative impacts on other users or the
    systems.

On login nodes, typical user tasks include

* Compiling codes (limit the number of threads, e.g., `make -j 8`)
* Editing files
* Submitting [jobs](../jobs/index.md)

Some workflows require interactive use of applications such as IDL,
MATLAB, NCL, python, and ROOT. For **small** datasets and **short**
runtimes it is acceptable to run these on login nodes. For extended
runtimes or large datasets these should be run in the batch queues.

## Policy

NERSC has implemented usage limits on Cori login nodes via Linux
cgroup limits.  These usage limits prevent inadvertent overuse of
resources and ensure a better interactive experience for all NERSC
users.

The following memory and CPU limits have been put in place on a
per-user basis (i.e., all processes combined from each user) on Cori.

| Node type | memory limit | CPU limit |
|-----------|--------------|-----------|
| login     | 128 GB       | 50%       |
| workflow  | 128 GB       | 50%       |
| jupyter   | 42 GB        | 50%       |

!!! note
    Processes will be throttled to CPU limits

!!! warning
    Processes may be terminated with a message like "Out of memory"
    when exceeding memory limits.

!!! warning "Avoid `watch`"
    If you _must_ use the `watch` command, please use a much longer
    interval such as 5 minutes (=300 sec), e.g., `watch -n 300
    <your_command>`.

## Tips

!!! tip "NERSC provides a wide variety of qos's"
    * An [interactive qos](../jobs/examples/index.md#interactive) is
    available on Cori for compute- and memory-intensive interactive
    work.
    * If you need to do a large number of data transfers use the dedicated
    [xfer queue](../jobs/examples/index.md#xfer-queue).

!!! tip
    To help identify processes that make heavy use of resources, you
    can use:

    * `top -u $USER`
    * `/usr/bin/time -v ./my_command`

!!! tip
    On Jupyter, there is a widget on the lower left-hand side of the
    JupyterLab UI that shows aggregate memory usage.
    
To access Cori system, see [Connecting to Cori Login Nodes](../connect/index.md). 
