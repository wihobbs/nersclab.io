# MPI

The Message Passing Interface Standard (MPI) is a message passing
library standard based on the consensus of the MPI Forum, which has
over 40 participating organizations, including vendors, researchers,
software library developers, and users. The goal of the Message
Passing Interface is to establish a portable, efficient, and flexible
standard for message passing that will be widely used for writing
message passing programs. As such, MPI is the first standardized,
vendor independent, message passing library. The advantages of
developing message passing software using MPI closely match the design
goals of portability, efficiency, and flexibility. MPI is not an IEEE
or ISO standard, but has in fact, become the "industry standard" for
writing message passing programs on HPC platforms.

## CUDA-Aware MPI

HPE Cray MPI is a CUDA-aware MPI implementation. This means that
the programmer can use pointers to GPU device memory in MPI buffers,
and the MPI implementation will correctly copy the data in GPU
device memory to/from the network interface card's (NIC's) memory,
either by implicitly copying the data first to host memory and then
copying the data from host memory to the NIC; or, in hardware which
supports [GPUDirect
RDMA](https://on-demand.gputechconf.com/gtc/2016/presentation/s6264-davide-rossetti-GPUDirect.pdf),
the data will be copied directly from the GPU to the NIC, bypassing
host memory altogether.

An example of this is shown below:

```C
#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>
#include <cuda_runtime.h>

int main(int argc, char *argv[]) {
    int myrank;
    float *val_device, *val_host;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

    val_host = (float*)malloc(sizeof(float));
    cudaMalloc((void **)&val_device, sizeof(float));

    *val_host = -1.0;
    if (myrank != 0) {
      printf("%s %d %s %f\n", "I am rank", myrank, "and my initial value is:", *val_host);
    }

    if (myrank == 0) {
        *val_host = 42.0;
        cudaMemcpy(val_device, val_host, sizeof(float), cudaMemcpyHostToDevice);
        printf("%s %d %s %f\n", "I am rank", myrank, "and will broadcast value:", *val_host);
    }

    MPI_Bcast(val_device, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

    if (myrank != 0) {
      cudaMemcpy(val_host, val_device, sizeof(float), cudaMemcpyDeviceToHost);
      printf("%s %d %s %f\n", "I am rank", myrank, "and received broadcasted value:", *val_host);
    }

    cudaFree(val_device);
    free(val_host);

    MPI_Finalize();
    return 0;
}
```

The above C code can be compiled with HPE Cray MPI using:

```
cc -o cuda-aware-bcast.ex cuda-aware-bcast.c
```

```console
$ export MPICH_GPU_SUPPORT_ENABLED=1
$ srun -C gpu -n 4 -G 4 --exclusive ./cuda-aware-bcast.ex
I am rank 1 and my initial value is: -1.000000
I am rank 1 and received broadcasted value: 42.000000
I am rank 0 and will broadcast value: 42.000000
I am rank 2 and my initial value is: -1.000000
I am rank 2 and received broadcasted value: 42.000000
I am rank 3 and my initial value is: -1.000000
I am rank 3 and received broadcasted value: 42.000000
```

The `MPICH_GPU_SUPPORT_ENABLED` environment variable must be enabled
if the application is CUDA-aware. Please read the `intro_mpi` man
page.

## Resources

* [LLNL Tutorials](https://computing.llnl.gov/tutorials/mpi/)
* [MPI Forum (standards body)](https://mpi-forum.org)
