# Perlmutter Timeline

This page records a brief timeline of significant events and user
environment changes on Perlmutter.

The following diagram shows a preliminary, approximate timeline for 
Perlmutter installation and access. Please note that dates are estimates
only, and may change.

![Perlmutter_timeline](../images/perlmutter-timeline.png)

## June, 2021

Perlmutter achieved 64.6 Pflop/s, putting it at No. 5 in the [Top500
list](https://www.top500.org/lists/top500/2021/06/).

## May 27, 2021

Perlmutter supercomputer dedication.

## November, 2020 - March, 2021
 
Perlmutter Phase 1 delivered.
