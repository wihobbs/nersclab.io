#!/bin/bash
#SBATCH -J test_mvasp
#SBATCH -N 64 
#SBATCH -C haswell 
#SBATCH -q debug
#SBATCH -o %x-%j.out
#SBATCH -t 30:00

module load mvasp/5.4.4-hsw

#run 512 VASP jobs simultaneously each running vasp_std with 4 processors (each node runs 8 jobs) 
srun -n 2048 -c2 --cpu-bind=cores ./mvasp_std


